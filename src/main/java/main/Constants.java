package main;

public final class Constants {

    public static final String WEBDRIVER_OPERA_DRIVER = "webdriver.opera.driver";
    public static final String WEBDRIVER_FIREFOX_DRIVER = "webdriver.gecko.driver";
    public static final String OPERA_DRIVER = "D:\\drivers\\operadriver.exe";
    public static final String FIREFOX_FIREFOX = "D:\\drivers\\geckodriver.exe";

    private Constants() {
        throw new IllegalStateException("Utility class");
    }
}
