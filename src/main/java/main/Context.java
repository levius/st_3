package main;


public class Context {
    private String geckodriver;
    private String operadriver;

    public String getGeckodriver() {
        return geckodriver;
    }

    public void setGeckodriver(String geckodriver) {
        this.geckodriver = geckodriver;
    }

    public String getOperadriver() {
        return operadriver;
    }

    public void setOperadriver(String operadriver) {
        this.operadriver = operadriver;
    }
}
