package main.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class AccountPage extends Page {

    private final String url = "https://www.multitran.com/";
    private final String login = "zayka98";
    private final String password = "zayka98zayka98";

    private final String info = "Обожаю собирать грибы и ягоды.";
    private final String accountPageButtonOpera = "//*[@id=\"start\"]/div[3]/div/a[1]";
    private final String accountPageButton = "/html/body/div/div[3]/div/a[1]";

    private final String clearDescriptionButtonFirefox = "//a[contains(text(),'Изменить данные профиля')]";
    private final String clearDescriptionButtonOpera = "//a[contains(text(),'Изменить данные профиля')]";

    private final String changeDescriptionButtonFirefix = "//a[contains(text(),'Изменить данные профиля')]";
    private final String changeDescriptionButtonOpera = "//a[contains(text(),'Изменить данные профиля')]";

    private final String descriptionTextFieldFirefox = "/html/body/div/div[5]/form/table/tbody/tr[1]/td[2]/textarea";
    private final String descriptionTextFieldOpera = "//*[@id=\"start\"]/div[5]/form/table/tbody/tr[1]/td[2]/textarea";

    private final String passwordFieldFirefox = "/html/body/div/div[5]/form/table/tbody/tr[2]/td[2]/input";
    private final String passwordFieldOpera ="//*[@id=\"start\"]/div[5]/form/table/tbody/tr[2]/td[2]/input";

    private final String saveLabelOpera = "/html/body/div/div[5]/div[1]/font";
    private final String saveLabelFirefox = "//*[@id=\"start\"]/div[5]/div[1]/font";

    private final String saveButtonOpera = "//*[@id=\"start\"]/div[5]/form/table/tbody/tr[4]/td[2]/input";
    private final String saveButtonFirefox = "/html/body/div/div[5]/form/table/tbody/tr[4]/td[2]/input";

    private final String resultLabelFirefox = "/html/body/div/div[5]/div[3]";
    private final String resultLabelOpera = "//*[@id=\"start\"]/div[5]/div[3]";


    public String getUrl() {
        return url;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public AccountPage(WebDriver driver) {
        super(driver);
    }

    public String getClearDescriptionButtonFirefox() {
        return clearDescriptionButtonFirefox;
    }

    public String getClearDescriptionButtonOpera() {
        return clearDescriptionButtonOpera;
    }

    public String getChangeDescriptionButtonFirefox() {
        return changeDescriptionButtonFirefix;
    }

    public String getDescriptionTextFieldFirefox() {
        return descriptionTextFieldFirefox;
    }

    public String getPasswordFieldFirefox() {
        return passwordFieldFirefox;
    }

    public String getPasswordFieldOpera() {
        return passwordFieldOpera;
    }

    public String getResultLabelFirefox() {
        return resultLabelFirefox;
    }

    public String getResultLabelOpera() {
        return resultLabelOpera;
    }

    public String getAccountPageButtonOpera() {
        return accountPageButtonOpera;
    }

    public String getChangeDescriptionButtonOpera() {
        return changeDescriptionButtonOpera;
    }

    public String getSaveButtonOpera() {
        return saveButtonOpera;
    }

    public String getSaveButtonFirefox() {
        return saveButtonFirefox;
    }

    public String getSaveLabelOpera() {
        return saveLabelOpera;
    }

    public String getSaveLabelFirefox() {
        return saveLabelFirefox;
    }

    public String getDescriptionTextFieldOpera() {
        return descriptionTextFieldOpera;
    }

    public String getAccountPageButton() {
        return accountPageButton;
    }

    public String getInfo() {
        return info;
    }


    public void doLogin(WebDriver driver, String login, String password){
        new StartPage(driver).doLogin( driver,  login,  password);
    }

    public void changeInfo(WebDriver driver, String info){
        WebDriverWait wait = new WebDriverWait(driver,20);
        if (driver instanceof FirefoxDriver) {
            driver.findElement(By.xpath(this.getAccountPageButton())).click(); // заходим в личный кабинет
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(this.getClearDescriptionButtonFirefox())));
            driver.findElement(By.xpath(this.getChangeDescriptionButtonFirefox())).click(); // нажимаем на изменение описания
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(this.getDescriptionTextFieldFirefox())));
            WebElement input = driver.findElement(By.xpath(this.getDescriptionTextFieldFirefox())); // вводим информацию
            input.clear();
            input.sendKeys(info);

            WebElement inputPass = driver.findElement(By.xpath(this.getPasswordFieldFirefox())); // вводим пароль
            inputPass.sendKeys(this.getPassword());

            driver.findElement(By.xpath(this.getSaveButtonFirefox())).click(); // нажатие на кнопку сохранить

        } else {
            driver.findElement(By.xpath(this.getAccountPageButtonOpera())).click();
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(this.getClearDescriptionButtonOpera())));
            driver.findElement(By.xpath(this.getChangeDescriptionButtonOpera())).click();

            WebElement input = driver.findElement(By.xpath(this.getDescriptionTextFieldOpera()));
            input.clear();
            input.sendKeys(info);

            WebElement inputPass = driver.findElement(By.xpath(this.getPasswordFieldOpera()));
            inputPass.sendKeys(this.getPassword());

            driver.findElement(By.xpath(this.getSaveButtonOpera())).click();
        }
    }


    public void clearInfo(WebDriver driver){
        WebDriverWait wait = new WebDriverWait(driver,20);
        if (driver instanceof FirefoxDriver) {
            driver.findElement(By.xpath(this.getAccountPageButton())).click(); // заходим в личный кабинет
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(this.getClearDescriptionButtonFirefox())));
            driver.findElement(By.xpath(this.getClearDescriptionButtonFirefox())).click(); // нажимаем на изменение описания
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(this.getDescriptionTextFieldFirefox())));
            WebElement input = driver.findElement(By.xpath(this.getDescriptionTextFieldFirefox())); // вводим информацию
            input.sendKeys(info);
            input.clear();

            WebElement inputPass = driver.findElement(By.xpath(this.getPasswordFieldFirefox())); // вводим пароль
            inputPass.sendKeys(this.getPassword());

            driver.findElement(By.xpath(this.getSaveButtonFirefox())).click(); // нажатие на кнопку сохранить

        } else {
            driver.findElement(By.xpath(this.getAccountPageButtonOpera())).click();
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(this.getClearDescriptionButtonOpera())));
            driver.findElement(By.xpath(this.getClearDescriptionButtonOpera())).click();

            WebElement input = driver.findElement(By.xpath(this.getDescriptionTextFieldOpera()));
            input.sendKeys(info);
            input.clear();

            WebElement inputPass = driver.findElement(By.xpath(this.getPasswordFieldOpera()));
            inputPass.sendKeys(this.getPassword());

            driver.findElement(By.xpath(this.getSaveButtonOpera())).click();
        }
    }

}