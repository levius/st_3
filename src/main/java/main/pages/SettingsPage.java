package main.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SettingsPage extends Page {

    private final String login = "zayka98";
    private final String password = "zayka98zayka98";

    private final String settingsButtonFirefox = "/html/body/div/div[3]/div/a[4]";
    private final String backgroundCheckBoxFirefox = "/html/body/div/div[5]/form/table/tbody/tr[11]/td/input";
    private final String settingsSaveButtonFirefox = "/html/body/div/div[5]/form/table/tbody/tr[18]/td/input";

    private final String settingsButtonOpera = "//*[@id=\"start\"]/div[3]/div/a[4]";
    private final String backgroundCheckBoxOpera = "//*[@id=\"start\"]/div[5]/form/table/tbody/tr[11]/td/input";
    private final String settingsSaveButtonOpera = "//*[@id=\"start\"]/div[5]/form/table/tbody/tr[18]/td/input";

    private final String accountNameF = "//*[@id=\"start\"]/div[3]/div/a[1]";
    private final String accountNameOpera = "//*[@id=\"start\"]/div[3]/div/a[1]";

    public String getAccountNameF() {
        return accountNameF;
    }

    public String getAccountNameOpera() {
        return accountNameOpera;
    }

    public String getSettingsButtonFirefox() {
        return settingsButtonFirefox;
    }

    public String getBackgroundCheckBoxFirefox() {
        return backgroundCheckBoxFirefox;
    }

    public String getSettingsSaveButtonFirefox() {
        return settingsSaveButtonFirefox;
    }

    public String getSettingsButtonOpera() {
        return settingsButtonOpera;
    }

    public String getBackgroundCheckBoxOpera() {
        return backgroundCheckBoxOpera;
    }

    public String getSettingsSaveButtonOpera() {
        return settingsSaveButtonOpera;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public SettingsPage(WebDriver driver) {
        super(driver);
    }

    public void doLogin(WebDriver driver, String login, String password) {
        new StartPage(driver).doLogin(driver, login, password);
    }

    public void changeSettings(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver, 20);
        if (driver instanceof FirefoxDriver) {
            driver.findElement(By.xpath(this.getSettingsButtonFirefox())).click(); // нажатие на кнопку настройки
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(this.getBackgroundCheckBoxFirefox())));
            driver.findElement(By.xpath(this.getBackgroundCheckBoxFirefox())).click(); // нажатие на чекбокс Отображать фоновый рисунок
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(this.getSettingsSaveButtonFirefox())));
            driver.findElement(By.xpath(this.getSettingsSaveButtonFirefox())).click(); // нажатие на кнопку сохранить
            wait.until(ExpectedConditions.textToBe(By.xpath(this.getAccountNameF()), this.getLogin()));

        } else {
            driver.findElement(By.xpath(this.getSettingsButtonOpera())).click(); // нажатие на кнопку настройки
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(this.getBackgroundCheckBoxOpera())));
            driver.findElement(By.xpath(this.getBackgroundCheckBoxOpera())).click(); // нажатие на чекбокс Отображать фоновый рисунок
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(this.getSettingsSaveButtonOpera())));
            driver.findElement(By.xpath(this.getSettingsSaveButtonOpera())).click(); // нажатие на кнопку сохранить
            wait.until(ExpectedConditions.textToBe(By.xpath(this.getAccountNameOpera()), this.getLogin()));
        }
    }


}