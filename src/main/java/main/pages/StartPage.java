package main.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.*;

import java.util.concurrent.TimeUnit;

/**
 * Created by i.isaev on 14.04.2021.
 */
public class StartPage extends Page {

    private final String url = "https://www.multitran.com/";
    private final String login = "zayka98";
    private final String password = "zayka98zayka98";

    public String getSignInButton() {
        return signInButton;
    }

    private final String signInButton = "//*[@id=\"start\"]/div[3]/div/a[1]";



    private final String loginInputOpera = "//*[@id=\"start\"]/div[5]/form/table/tbody/tr[1]/td[2]/input";
    private final String passwordInputOpera = "//*[@id=\"start\"]/div[5]/form/table/tbody/tr[2]/td[2]/input";
    private final String signInButtonOpera = "//*[@id=\"start\"]/div[5]/form/table/tbody/tr[4]/td/input";


    private final String loginInputFirefox = "/html/body/div/div[5]/form/table/tbody/tr[1]/td[2]/input";
    private final String passwordInputFirefox = "/html/body/div/div[5]/form/table/tbody/tr[2]/td[2]/input";
    private final String signInButtonFirefox = "/html/body/div/div[5]/form/table/tbody/tr[4]/td/input";

    private final String accountNameF = "//*[@id=\"start\"]/div[3]/div/a[1]";
    private final String accountNameOpera = "//*[@id=\"start\"]/div[3]/div/a[1]";

    private final String logoutBtnF = "//*[@id=\"start\"]/div[3]/div/a[2]";
    private final String logoutBtnOpera = "//*[@id=\"start\"]/div[3]/div/a[2]";

    private final String wrongPasswordLabel = "/html/body/div/div[5]/table/tbody/tr[1]/td/strong/font";
    private final String wrongPasswordLabelOpera = "\"//*[@id=\\\"start\\\"]/div[5]/table/tbody/tr[1]/td/strong/font\"";

    private final String languageSelect1 = "/html/body/div/div[5]/form/select[1]";
    private final String languageSelect2 = "/html/body/div/div[5]/form/select[2]";

    private final String language1 = "Russian";
    private final String language2 = "French";

    private final String defaultQuery = "ромашка";

    private final String searchResultFirefox = "/html/body/div/div[5]/table[1]/tbody/tr[4]/td[2]/a";
    private final String searchResultChrome = "/html/body/div/div[5]/table[1]/tbody/tr[4]/td[2]/a";

    private final String addLabelFirefox = "/html/body/div[1]/div[5]/div[1]/div[1]/a[1]";
    private final String addLabelChrome = "/html/body/div[1]/div[5]/div[1]/div[1]/a[1]";

    public String getAddLabelFirefox() {
        return addLabelFirefox;
    }
    public String getLoginInputOpera() {
        return loginInputOpera;
    }

    public String getPasswordInputOpera() {
        return passwordInputOpera;
    }

    public String getSignInButtonOpera() {
        return signInButtonOpera;
    }

    public String getAddLabelChrome() {
        return addLabelChrome;
    }

    public String getWrongPasswordLabelOpera() {
        return wrongPasswordLabelOpera;
    }

    public String getAccountNameOpera() {
        return accountNameOpera;
    }

    public String getLogoutBtnOpera() {
        return logoutBtnOpera;
    }

    public String getSearchResultFirefox() {
        return searchResultFirefox;
    }

    public String getSearchResultChrome() {
        return searchResultChrome;
    }

    public String getUrl() {
        return url;
    }

    public String getDefaultQuery() {
        return defaultQuery;
    }

    public String getLanguageSelect1() {
        return languageSelect1;
    }

    public String getLanguageSelect2() {
        return languageSelect2;
    }

    public String getLanguage1() {
        return language1;
    }

    public String getLanguage2() {
        return language2;
    }

    public String getWrongPasswordLabel() {
        return wrongPasswordLabel;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public String getAccountNameF() {
        return accountNameF;
    }

    public String getLogoutBtnF() {
        return logoutBtnF;
    }

    public String getLoginInputFirefox() {
        return loginInputFirefox;
    }

    public String getPasswordInputFirefox() {
        return passwordInputFirefox;
    }

    public String getSignInButtonFirefox() {
        return signInButtonFirefox;
    }



    public StartPage(WebDriver driver) {
        super(driver);
    }

    public String doLogin(WebDriver driver, String login, String password) {
        driver.get(this.getUrl());
        String originalWindow;
        WebDriverWait wait = new WebDriverWait(driver,20);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(this.getSignInButton())));
        driver.findElement(By.xpath(this.getSignInButton())).click();
        originalWindow = driver.getWindowHandle();
        if (driver instanceof FirefoxDriver) {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(this.getLoginInputFirefox())));
            driver.findElement(By.xpath(this.getLoginInputFirefox())).sendKeys(login);
            driver.findElement(By.xpath(this.getPasswordInputFirefox())).sendKeys(password);
            driver.findElement(By.xpath(this.getSignInButtonFirefox())).click();
        } else {
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(this.getLoginInputOpera())));
            driver.findElement(By.xpath(this.getLoginInputOpera())).sendKeys(login);
            driver.findElement(By.xpath(this.getPasswordInputOpera())).sendKeys(password);
            driver.findElement(By.xpath(this.getSignInButtonOpera())).click();
        }
        return originalWindow;
    }

    public boolean elementExists(WebDriver driver, By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }
//TODO сделать отдельную часть для firefox и опера

    public void selectLanguages(WebDriver driver) {
        WebDriverWait wait = new WebDriverWait(driver,20);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(this.getLanguageSelect1())));
        WebElement selectLanguage1 = driver.findElement(By.xpath(this.getLanguageSelect1())); // выбираем русский язык из выпадающего списка
        Select selectLanguageSelect1 = new Select(selectLanguage1);
        selectLanguageSelect1.selectByVisibleText(this.getLanguage1());
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(this.getLanguageSelect2())));
        WebElement selectLanguage2 = driver.findElement(By.xpath(this.getLanguageSelect2())); // выбираем испанский язык из выпадающего списка
        Select selectLanguageSelect2 = new Select(selectLanguage2);
        selectLanguageSelect2.selectByVisibleText(this.getLanguage2());
    }

    public void doSearch(WebDriver driver, String query) {
        WebElement searchBar = driver.findElement(By.xpath("//*[@id=\"s\"]"));
        searchBar.sendKeys(query);
        searchBar.sendKeys(Keys.ENTER);
    }

}