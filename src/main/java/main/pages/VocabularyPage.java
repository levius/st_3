package main.pages;

import org.openqa.selenium.WebDriver;

public class VocabularyPage extends Page {

    private final String url = "https://www.multitran.com/";

    private final String engRusFireFox ="/html/body/div/div[5]/table/tbody/tr[3]/td[1]/a[1]" ;
    private final String engRusOpera = "/html/body/div/div[5]/table/tbody/tr[3]/td[1]/a[1]";

    private final String brisilia =     "/html/body/div/div[5]/table[2]/tbody/tr[105]/td[1]/a";
    private final String bibliography =  "//*[@id=\"start\"]/div[5]/table[2]/tbody/tr[105]/td[1]/a";

    private final String brisiliaButton = "/html/body/div/div[5]/table[2]/tbody/tr[16]/td[2]/a";
//    private final String oklad = "//*[@id=\"start\"]/div[5]/table[2]/tbody/tr[16]/td[2]/a";
    private  final String oklad = "//a[contains(.,'оклад')]";
    private final String resultFirefox = "/html/body/div/div[5]/table[1]/tbody/tr[3]/td[2]/a[1]";
    private final String resultOpera = "//*[@id=\"start\"]/div[5]/table[1]/tbody/tr[3]/td[2]/a[1]";

    private final String checkWord = "remuneration";

    public String getBrisilia() {
        return brisilia;
    }

    public String getBibliography() {
        return bibliography;
    }

    public String getCheckWord() {
        return checkWord;
    }

    public String getBrisiliaButton() {
        return brisiliaButton;
    }

    public String getOklad() {
        return oklad;
    }

    public String getResultFirefox() {
        return resultFirefox;
    }

    public String getResultOpera() {
        return resultOpera;
    }

    public String getEngRusFireFox() {
        return engRusFireFox;
    }

    public String getEngRusOpera() {
        return engRusOpera;
    }

    public VocabularyPage(WebDriver driver) {
        super(driver);
    }

    public String getUrl() {
        return url;
    }
}