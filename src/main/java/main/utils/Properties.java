package main.utils;

import org.apache.log4j.Logger;
import main.*;
import main.exceptions.*;

import java.io.FileInputStream;
import java.io.IOException;


public class Properties {

    private static final Logger logger = Logger.getLogger(Properties.class);

    private static Properties instance;

    public static Properties getInstance() {
        if (instance == null) {
            instance = new Properties();
        }
        return instance;
    }

    public void reading(Context context) {
        var props = new java.util.Properties();

        try {
            props.load(new FileInputStream("conf.properties"));

            if (props.getProperty(Constants.WEBDRIVER_OPERA_DRIVER) != null) {
                if (props.getProperty(Constants.WEBDRIVER_OPERA_DRIVER).equals(Constants.OPERA_DRIVER)) {
                    context.setOperadriver(props.getProperty(Constants.WEBDRIVER_OPERA_DRIVER));
                }
            } else {
//                throw new InvalidChromeException();
            }

            if (props.getProperty(Constants.WEBDRIVER_FIREFOX_DRIVER) != null) {
                if (props.getProperty(Constants.WEBDRIVER_FIREFOX_DRIVER).equals(Constants.FIREFOX_FIREFOX)) {
                    context.setGeckodriver(props.getProperty(Constants.WEBDRIVER_FIREFOX_DRIVER));
                }
            } else {
//                throw new InvalidFirefoxException();
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }
}
