import main.pages.StartPage;

import org.apache.log4j.Logger;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;

import main.*;
import main.exceptions.InvalidPropertiesException;
import main.utils.Properties;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.*;

import static org.junit.Assert.assertTrue;



public class AccountLoginTest
{

    private static final Logger logger = Logger.getLogger(AccountLoginTest.class);

    public Context context;
    public List<WebDriver> driverList;

    @BeforeEach
    public void setUp() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        context = new Context();
        driverList = new ArrayList<>();
        try {
            Properties.getInstance().reading(context);
        } catch (IllegalArgumentException e) {
            logger.error(e.getMessage());
        }

        if (context.getGeckodriver() != null) {
            System.setProperty(Constants.WEBDRIVER_FIREFOX_DRIVER, context.getGeckodriver());
            driverList.add(new FirefoxDriver());
        }
        if (context.getOperadriver() != null) {
            System.setProperty(Constants.WEBDRIVER_OPERA_DRIVER, context.getOperadriver());
            driverList.add(new OperaDriver());
        }
        if (driverList.isEmpty()) throw new InvalidPropertiesException();
    }

//TODO добавить ожидание кнопки выйти, перед выходом, чтобы обойти рекламу
    @Test
    @Order(1)
    public void correctLoginTest() {
        driverList.parallelStream().forEach(webDriver -> {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            WebDriverWait wait = new WebDriverWait(webDriver, 20);
            StartPage startPage = new StartPage(webDriver);
            startPage.doLogin(webDriver,startPage.getLogin(),startPage.getPassword());

            if (webDriver instanceof FirefoxDriver) {
                wait.until(ExpectedConditions.textToBe(By.xpath(startPage.getAccountNameF()), startPage.getLogin())); // проверяем инициалы аккаунта в правом верхнем углу
                WebElement exitBtn = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(startPage.getLogoutBtnF()))); // жмем кнопку выйти
                exitBtn.click();
            } else {
                wait.until(ExpectedConditions.textToBe(By.xpath(startPage.getAccountNameOpera()), startPage.getLogin())); // проверяем инициалы аккаунта в правом верхнем углу
                WebElement exitBtn = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(startPage.getLogoutBtnOpera()))); // жмем кнопку выйти
                exitBtn.click();
            }
            webDriver.quit();
        });
    }

    @Test
    @Order(2)
    public void incorrectLoginTest() {
        driverList.parallelStream().forEach(webDriver -> {
//            try {
//                Thread.sleep(5000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
            StartPage startPage = new StartPage(webDriver);
            String window = startPage.doLogin(webDriver,startPage.getLogin(),"НЕВЕРНЫЙ ПАРОЛЬ");
            webDriver.switchTo().window(window);

            if (webDriver instanceof FirefoxDriver) {
                assertTrue(startPage.elementExists(webDriver, By.xpath(startPage.getWrongPasswordLabel())));
            } else {
                assertTrue(startPage.elementExists(webDriver, By.xpath(startPage.getWrongPasswordLabel())));
            }
            webDriver.quit();
        });
    }
}
