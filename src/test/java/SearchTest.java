import main.Constants;
import main.Context;

import main.exceptions.InvalidPropertiesException;
import main.pages.StartPage;
import main.utils.Properties;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class SearchTest {

    public Context context;
    public List<WebDriver> driverList;
    private static final Logger logger = Logger.getLogger(SearchTest.class);
    @BeforeEach
    public void setUp() {
        context = new Context();
        driverList = new ArrayList<>();
        try {
            Properties.getInstance().reading(context);
        } catch (IllegalArgumentException e) {
            logger.error(e.getMessage());
        }

        if (context.getGeckodriver() != null) {
            System.setProperty(Constants.WEBDRIVER_FIREFOX_DRIVER, context.getGeckodriver());
            driverList.add(new FirefoxDriver());
        }
        if (context.getOperadriver() != null) {
            System.setProperty(Constants.WEBDRIVER_OPERA_DRIVER, context.getOperadriver());
            driverList.add(new OperaDriver());
        }
        if (driverList.isEmpty()) throw new InvalidPropertiesException();
    }


    @Test
    public void correctSearchTest() {
        driverList.forEach(webDriver -> {
            WebDriverWait wait = new WebDriverWait(webDriver, 10);
            StartPage startPage = new StartPage(webDriver);
            webDriver.get(startPage.getUrl());
            startPage.selectLanguages(webDriver);
            startPage.doSearch(webDriver, startPage.getDefaultQuery());

            WebElement result;
            if (webDriver instanceof FirefoxDriver) {
                (new WebDriverWait(webDriver, 5)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(startPage.getSearchResultFirefox())));
                result = webDriver.findElement(By.xpath(startPage.getSearchResultFirefox()));
            } else {
                (new WebDriverWait(webDriver, 5)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(startPage.getSearchResultChrome())));
                result = webDriver.findElement(By.xpath(startPage.getSearchResultChrome()));
            }

            assertEquals("matricaire", result.getText());
            webDriver.quit();
        });
    }


    @Test
    public void incorrectSearchTest() {
        driverList.parallelStream().forEach(webDriver -> {
            WebDriverWait wait = new WebDriverWait(webDriver, 10);
            StartPage startPage = new StartPage(webDriver);
            webDriver.get(startPage.getUrl());
            startPage.selectLanguages(webDriver);
            startPage.doSearch(webDriver, "алигароарорнаяпушкабароназвяне");

            WebElement result;
            if (webDriver instanceof FirefoxDriver) {
                (new WebDriverWait(webDriver, 5)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(startPage.getAddLabelFirefox())));
                result = webDriver.findElement(By.xpath(startPage.getAddLabelFirefox()));
            } else {
                (new WebDriverWait(webDriver, 5)).until(ExpectedConditions.presenceOfElementLocated(By.xpath(startPage.getAddLabelChrome())));
                result = webDriver.findElement(By.xpath(startPage.getAddLabelChrome()));
            }

            assertEquals("Add", result.getText());
            webDriver.quit();
        });
    }
}