import main.Constants;
import main.Context;
import main.exceptions.InvalidPropertiesException;
import main.pages.SettingsPage;
import main.utils.Properties;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

public class SettingsTest {

    public Context context;
    public List<WebDriver> driverList;
    private static final Logger logger = Logger.getLogger(SettingsTest.class);
    @BeforeEach
    public void setUp() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        context = new Context();
        driverList = new ArrayList<>();
        try {
            Properties.getInstance().reading(context);
        } catch (IllegalArgumentException e) {
            logger.error(e.getMessage());
        }

        if (context.getGeckodriver() != null) {
            System.setProperty(Constants.WEBDRIVER_FIREFOX_DRIVER, context.getGeckodriver());
            driverList.add(new FirefoxDriver());
        }
        if (context.getOperadriver() != null) {
            System.setProperty(Constants.WEBDRIVER_OPERA_DRIVER, context.getOperadriver());
            driverList.add(new OperaDriver());
        }
        if (driverList.isEmpty()) throw new InvalidPropertiesException();
    }

    @Test
    public void changeBackgroundTest() {
        driverList.forEach(webDriver -> {
            WebDriverWait wait = new WebDriverWait(webDriver, 50);
            SettingsPage settingsPage = new SettingsPage(webDriver);

            settingsPage.doLogin(webDriver,settingsPage.getLogin(),settingsPage.getPassword());

            var background = webDriver.findElement(By.xpath("/html/body"));
            String previousStyle = (background.getAttribute("style"));
            settingsPage.changeSettings(webDriver);

            background = webDriver.findElement(By.xpath("/html/body"));
            if (previousStyle.equals("background-image: url(\"/gif/bg.gif\");")){
                wait.until(ExpectedConditions.attributeToBe(background,"style", "background-image: none;"));
            }else{
                wait.until(ExpectedConditions.attributeToBe(background,"style", "background-image: url(\"/gif/bg.gif\");"));
            }
            webDriver.quit();
        });
    }

}