import main.Constants;
import main.Context;
import main.exceptions.InvalidPropertiesException;
import main.utils.Properties;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import main.pages.VocabularyPage;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;

public class VocabTest {

    public Context context;
    public List<WebDriver> driverList;
    private static final Logger logger = Logger.getLogger(VocabTest.class);

    @BeforeEach
    public void setUp() {
        context = new Context();
        driverList = new ArrayList<>();

        try {
            Properties.getInstance().reading(context);
        } catch (IllegalArgumentException e) {
            logger.error(e.getMessage());
        }

        if (context.getGeckodriver() != null) {
            System.setProperty(Constants.WEBDRIVER_FIREFOX_DRIVER, context.getGeckodriver());
            driverList.add(new FirefoxDriver());
        }
        if (context.getOperadriver() != null) {
            System.setProperty(Constants.WEBDRIVER_OPERA_DRIVER, context.getOperadriver());
            driverList.add(new OperaDriver());
        }
        if (driverList.isEmpty()) throw new InvalidPropertiesException();
    }

    @Test
    public void vocabulariesTest() {
        driverList.parallelStream().forEach(webDriver -> {
            WebDriverWait wait = new WebDriverWait(webDriver, 20);
            VocabularyPage vocabularyPage = new VocabularyPage(webDriver);
            webDriver.get(vocabularyPage.getUrl());

            if (webDriver instanceof FirefoxDriver) {
                webDriver.findElement(By.xpath(vocabularyPage.getEngRusFireFox())).click(); // нажатие на англо русский словарь
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(vocabularyPage.getBrisilia())));
                webDriver.findElement(By.xpath(vocabularyPage.getBrisilia())).click(); // нажатие на раздел Бразилия
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(vocabularyPage.getBrisiliaButton())));
                webDriver.findElement(By.xpath(vocabularyPage.getBrisiliaButton())).click(); // нажатие на слово Бразилия
                wait.until(ExpectedConditions.textToBe(By.xpath(vocabularyPage.getResultFirefox()), vocabularyPage.getCheckWord()));
            } else {
                webDriver.findElement(By.xpath(vocabularyPage.getEngRusOpera())).click(); // нажатие на англо русский словарь
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(vocabularyPage.getBibliography())));
                webDriver.findElement(By.xpath(vocabularyPage.getBibliography())).click(); // нажатие на раздел Bibliography
                wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(vocabularyPage.getOklad())));
                webDriver.findElement(By.xpath(vocabularyPage.getOklad())).click();
                wait.until(ExpectedConditions.textToBe(By.xpath(vocabularyPage.getResultOpera()), vocabularyPage.getCheckWord()));
            }
            webDriver.quit();
        });
    }


}