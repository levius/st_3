import main.pages.AccountPage;
import org.apache.log4j.Logger;
import org.junit.jupiter.api.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import main.*;
import main.exceptions.InvalidPropertiesException;
import main.utils.Properties;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.*;


public class WChangeInfoTest {

    public Context context;
    public List<WebDriver> driverList;
    private static final Logger logger = Logger.getLogger(WChangeInfoTest.class);
    @BeforeEach
    public void setUp() {
//        try {
//            Thread.sleep(2000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        context = new Context();
        driverList = new ArrayList<>();
        try {
            Properties.getInstance().reading(context);
        } catch (IllegalArgumentException e) {
            logger.error(e.getMessage());
        }

        if (context.getGeckodriver() != null) {
            System.setProperty(Constants.WEBDRIVER_FIREFOX_DRIVER, context.getGeckodriver());
            driverList.add(new FirefoxDriver());
        }
        if (context.getOperadriver() != null) {
            System.setProperty(Constants.WEBDRIVER_OPERA_DRIVER, context.getOperadriver());
            driverList.add(new OperaDriver());
        }
        if (driverList.isEmpty()) throw new InvalidPropertiesException();
    }


    @Test
    public void clearInfoTest() {
        driverList.forEach(webDriver -> {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            WebDriverWait wait = new WebDriverWait(webDriver, 10);
            AccountPage accountPage = new AccountPage(webDriver);
            accountPage.doLogin(webDriver,accountPage.getLogin(),accountPage.getPassword());
            accountPage.clearInfo(webDriver);
            if (webDriver instanceof FirefoxDriver) {
                wait.until(ExpectedConditions.textToBe(By.xpath(accountPage.getSaveLabelFirefox()), "Запись сохранена")); // нажатие на кнопку сохранить
            } else {
                wait.until(ExpectedConditions.textToBe(By.xpath(accountPage.getSaveLabelOpera()), "Запись сохранена"));
            }
            webDriver.quit();
        });
    }

    @Test
    public void changeInfoTest() {
        driverList.forEach(webDriver -> {
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            WebDriverWait wait = new WebDriverWait(webDriver, 10);
            AccountPage accountPage = new AccountPage(webDriver);
            accountPage.doLogin(webDriver,accountPage.getLogin(),accountPage.getPassword());
            accountPage.changeInfo(webDriver,accountPage.getInfo());
            if (webDriver instanceof FirefoxDriver) {
                wait.until(ExpectedConditions.textToBe(By.xpath(accountPage.getResultLabelFirefox()), accountPage.getInfo()));
            } else {
                wait.until(ExpectedConditions.textToBe(By.xpath(accountPage.getResultLabelOpera()), accountPage.getInfo()));
            }
            webDriver.quit();
        });
    }

}